# mouse-Dxz4

This repository contains the scripts for the analysis of DNaseHi-C, RNA-seq, CTCF ChIP-seq, and ATAC-seq data related to the article below. Using Hi-C in edited mouse cells with allelic deletions or inversions within the hinge, we show that the conserved Dxz4 locus is necessary to maintain this bipartite structure and its orientation controls the distribution of contacts on the Xi.

Bonora, G. and Deng, X. and Fang, H. and Ramani, V. and Qiu, R. and Berletch, J. B. and Filippova, G. N. and Duan, Z. and Shendure, J. and Noble, W. S. and Disteche, C. M. Orientation-dependent Dxz4 contacts shape the 3D structure of the inactive X chromosome. Nature Communications 9, no. 1 (April 13, 2018): 1445. https://doi.org/10.1038/s41467-018-03694-y.

All scripts are provided as is without warranty and at the user's own risk. This is not a software package release, but rather the provision of code to facilitate the reproduction our analyses. Future support of these scripts is not guaranteed.